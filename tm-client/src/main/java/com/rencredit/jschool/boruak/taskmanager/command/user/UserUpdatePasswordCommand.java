package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.User;
import com.rencredit.jschool.boruak.taskmanager.endpoint.UserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "update-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Update password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter new password");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @Nullable final String userId = authEndpoint.getUserId(session);
        @NotNull final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.updateUserPasswordById(session, userId, password);
        ViewUtil.showUser(user);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
