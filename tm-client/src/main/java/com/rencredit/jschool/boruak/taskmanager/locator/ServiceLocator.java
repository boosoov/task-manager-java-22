package com.rencredit.jschool.boruak.taskmanager.locator;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IInfoService;
import com.rencredit.jschool.boruak.taskmanager.repository.CommandRepository;
import com.rencredit.jschool.boruak.taskmanager.service.CommandService;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;

public class ServiceLocator implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IInfoService infoService = new InfoService(this);


    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IInfoService getInfoService() {
        return infoService;
    }
}
