package com.rencredit.jschool.boruak.taskmanager.command.info;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal command.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        System.out.println(serviceLocator.getInfoService().getHelp());
    }

}
