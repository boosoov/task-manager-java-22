package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRegistrationCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "registration";
    }

    @NotNull
    @Override
    public String description() {
        return "Registration in system.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @NotNull final boolean result = authEndpoint.registrationLoginPassword(login, password);
        if (result) System.out.println("OK");
        else System.out.println("FAIL");
    }

}
