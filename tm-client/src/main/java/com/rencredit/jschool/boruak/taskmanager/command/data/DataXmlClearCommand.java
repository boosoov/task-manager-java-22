package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.IOException_Exception;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataXmlClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove xml data.";
    }

    @Override
    public void execute() throws IOException_Exception {
        endpointLocator.getAdminEndpoint().clearXml(session);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
