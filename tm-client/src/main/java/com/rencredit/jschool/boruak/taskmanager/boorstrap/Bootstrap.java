package com.rencredit.jschool.boruak.taskmanager.boorstrap;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownCommandException;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.Set;

public class Bootstrap {

    @NotNull
    private final static IServiceLocator serviceLocator = new ServiceLocator();

    @NotNull
    private final static IEndpointLocator endpointLocator = new EndpointLocator();

    @NotNull
    private final static String pathCommands = "com.rencredit.jschool.boruak.taskmanager.command";

    @NotNull
    private final static Session session = new Session();

    public Bootstrap() {
    }

    public void run(@Nullable final String[] args) {
        @NotNull String[] commands = null;
        init();
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        parseArgs(args);
        while (true) {
            try {
                commands = TerminalUtil.nextLine().split("\\s+");
                parseArgs(commands);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void init() {
        registryCommand();
    }

    @SneakyThrows
    public void registryCommand() {
        @NotNull final Reflections reflections = new Reflections(pathCommands);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            @NotNull final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setEndpointLocator(endpointLocator, serviceLocator, session);
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        commandService.putCommand(command.name(), command);
    }


    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            processArg(TerminalCommandUtil.convertArgumentToCommand(arg));
            System.out.println();
        }
    }

    @SneakyThrows
    private void processArg(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @Nullable final AbstractCommand command = commandService.getTerminalCommands().get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    public IEndpointLocator getEndpointLocator() {
        return endpointLocator;
    }

    @NotNull
    public static Session getSession() {
        return session;
    }

}
