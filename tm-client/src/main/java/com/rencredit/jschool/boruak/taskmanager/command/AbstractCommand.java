package com.rencredit.jschool.boruak.taskmanager.command;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.ClassNotFoundException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.IOException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public abstract class AbstractCommand {

    @NotNull
    protected IEndpointLocator endpointLocator;

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    protected Session session;

    public AbstractCommand() {
    }

    public void setEndpointLocator(
            @NotNull IEndpointLocator endpointLocator,
            @NotNull IServiceLocator serviceLocator,
            @NotNull Session session
    ) {
        this.endpointLocator = endpointLocator;
        this.serviceLocator = serviceLocator;
        this.session = session;
    }

    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute() throws IOException, ClassNotFoundException, IOException_Exception, ClassNotFoundException_Exception;

}
