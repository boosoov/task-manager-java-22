package com.rencredit.jschool.boruak.taskmanager.command.user.auth;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user in program";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @NotNull final Session sessionFromServer = authEndpoint.logIn(login, password);
        session.setId(sessionFromServer.getId());
        session.setUserId(sessionFromServer.getUserId());
        session.setSignature(sessionFromServer.getSignature());
        session.setTimestamp(sessionFromServer.getTimestamp());
        System.out.println("Login success");
    }

}
