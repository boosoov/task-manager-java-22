
package com.rencredit.jschool.boruak.taskmanager.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rencredit.jschool.boruak.taskmanager.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckRoles_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "checkRoles");
    private final static QName _CheckRolesResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "checkRolesResponse");
    private final static QName _GetUserId_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserId");
    private final static QName _GetUserIdResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserIdResponse");
    private final static QName _LogIn_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logIn");
    private final static QName _LogInResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logInResponse");
    private final static QName _LogOut_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logOut");
    private final static QName _LogOutResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logOutResponse");
    private final static QName _RegistrationLoginPassword_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPassword");
    private final static QName _RegistrationLoginPasswordEmail_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordEmail");
    private final static QName _RegistrationLoginPasswordEmailResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordEmailResponse");
    private final static QName _RegistrationLoginPasswordResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordResponse");
    private final static QName _RegistrationLoginPasswordRole_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordRole");
    private final static QName _RegistrationLoginPasswordRoleResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordRoleResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rencredit.jschool.boruak.taskmanager.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckRoles }
     * 
     */
    public CheckRoles createCheckRoles() {
        return new CheckRoles();
    }

    /**
     * Create an instance of {@link CheckRolesResponse }
     * 
     */
    public CheckRolesResponse createCheckRolesResponse() {
        return new CheckRolesResponse();
    }

    /**
     * Create an instance of {@link GetUserId }
     * 
     */
    public GetUserId createGetUserId() {
        return new GetUserId();
    }

    /**
     * Create an instance of {@link GetUserIdResponse }
     * 
     */
    public GetUserIdResponse createGetUserIdResponse() {
        return new GetUserIdResponse();
    }

    /**
     * Create an instance of {@link LogIn }
     * 
     */
    public LogIn createLogIn() {
        return new LogIn();
    }

    /**
     * Create an instance of {@link LogInResponse }
     * 
     */
    public LogInResponse createLogInResponse() {
        return new LogInResponse();
    }

    /**
     * Create an instance of {@link LogOut }
     * 
     */
    public LogOut createLogOut() {
        return new LogOut();
    }

    /**
     * Create an instance of {@link LogOutResponse }
     * 
     */
    public LogOutResponse createLogOutResponse() {
        return new LogOutResponse();
    }

    /**
     * Create an instance of {@link RegistrationLoginPassword }
     * 
     */
    public RegistrationLoginPassword createRegistrationLoginPassword() {
        return new RegistrationLoginPassword();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordEmail }
     * 
     */
    public RegistrationLoginPasswordEmail createRegistrationLoginPasswordEmail() {
        return new RegistrationLoginPasswordEmail();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordEmailResponse }
     * 
     */
    public RegistrationLoginPasswordEmailResponse createRegistrationLoginPasswordEmailResponse() {
        return new RegistrationLoginPasswordEmailResponse();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordResponse }
     * 
     */
    public RegistrationLoginPasswordResponse createRegistrationLoginPasswordResponse() {
        return new RegistrationLoginPasswordResponse();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordRole }
     * 
     */
    public RegistrationLoginPasswordRole createRegistrationLoginPasswordRole() {
        return new RegistrationLoginPasswordRole();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordRoleResponse }
     * 
     */
    public RegistrationLoginPasswordRoleResponse createRegistrationLoginPasswordRoleResponse() {
        return new RegistrationLoginPasswordRoleResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AbstractEntity }
     * 
     */
    public AbstractEntity createAbstractEntity() {
        return new AbstractEntity();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "checkRoles")
    public JAXBElement<CheckRoles> createCheckRoles(CheckRoles value) {
        return new JAXBElement<CheckRoles>(_CheckRoles_QNAME, CheckRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "checkRolesResponse")
    public JAXBElement<CheckRolesResponse> createCheckRolesResponse(CheckRolesResponse value) {
        return new JAXBElement<CheckRolesResponse>(_CheckRolesResponse_QNAME, CheckRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserId")
    public JAXBElement<GetUserId> createGetUserId(GetUserId value) {
        return new JAXBElement<GetUserId>(_GetUserId_QNAME, GetUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserIdResponse")
    public JAXBElement<GetUserIdResponse> createGetUserIdResponse(GetUserIdResponse value) {
        return new JAXBElement<GetUserIdResponse>(_GetUserIdResponse_QNAME, GetUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogIn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logIn")
    public JAXBElement<LogIn> createLogIn(LogIn value) {
        return new JAXBElement<LogIn>(_LogIn_QNAME, LogIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logInResponse")
    public JAXBElement<LogInResponse> createLogInResponse(LogInResponse value) {
        return new JAXBElement<LogInResponse>(_LogInResponse_QNAME, LogInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogOut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logOut")
    public JAXBElement<LogOut> createLogOut(LogOut value) {
        return new JAXBElement<LogOut>(_LogOut_QNAME, LogOut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogOutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logOutResponse")
    public JAXBElement<LogOutResponse> createLogOutResponse(LogOutResponse value) {
        return new JAXBElement<LogOutResponse>(_LogOutResponse_QNAME, LogOutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPassword")
    public JAXBElement<RegistrationLoginPassword> createRegistrationLoginPassword(RegistrationLoginPassword value) {
        return new JAXBElement<RegistrationLoginPassword>(_RegistrationLoginPassword_QNAME, RegistrationLoginPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordEmail")
    public JAXBElement<RegistrationLoginPasswordEmail> createRegistrationLoginPasswordEmail(RegistrationLoginPasswordEmail value) {
        return new JAXBElement<RegistrationLoginPasswordEmail>(_RegistrationLoginPasswordEmail_QNAME, RegistrationLoginPasswordEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordEmailResponse")
    public JAXBElement<RegistrationLoginPasswordEmailResponse> createRegistrationLoginPasswordEmailResponse(RegistrationLoginPasswordEmailResponse value) {
        return new JAXBElement<RegistrationLoginPasswordEmailResponse>(_RegistrationLoginPasswordEmailResponse_QNAME, RegistrationLoginPasswordEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordResponse")
    public JAXBElement<RegistrationLoginPasswordResponse> createRegistrationLoginPasswordResponse(RegistrationLoginPasswordResponse value) {
        return new JAXBElement<RegistrationLoginPasswordResponse>(_RegistrationLoginPasswordResponse_QNAME, RegistrationLoginPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordRole")
    public JAXBElement<RegistrationLoginPasswordRole> createRegistrationLoginPasswordRole(RegistrationLoginPasswordRole value) {
        return new JAXBElement<RegistrationLoginPasswordRole>(_RegistrationLoginPasswordRole_QNAME, RegistrationLoginPasswordRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordRoleResponse")
    public JAXBElement<RegistrationLoginPasswordRoleResponse> createRegistrationLoginPasswordRoleResponse(RegistrationLoginPasswordRoleResponse value) {
        return new JAXBElement<RegistrationLoginPasswordRoleResponse>(_RegistrationLoginPasswordRoleResponse_QNAME, RegistrationLoginPasswordRoleResponse.class, null, value);
    }

}
