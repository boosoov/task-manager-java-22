package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.IOException_Exception;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ShutDownServerCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "shut-down-server";
    }

    @NotNull
    @Override
    public String description() {
        return "Shut down server.";
    }

    @Override
    public void execute() throws IOException_Exception {
        endpointLocator.getAdminEndpoint().shutDownServer(session);
    }

}
