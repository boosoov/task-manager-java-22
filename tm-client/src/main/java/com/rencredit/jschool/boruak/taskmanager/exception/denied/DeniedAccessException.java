package com.rencredit.jschool.boruak.taskmanager.exception.denied;

public class DeniedAccessException extends RuntimeException {

    public DeniedAccessException() {
        super("Access denied...");
    }

}
