package com.rencredit.jschool.boruak.taskmanager.exception.unknown;

public class UnknownUserException extends RuntimeException {

    public UnknownUserException() {
        super("Error! Unknown user...");
    }

    public UnknownUserException(final String user) {
        super("Error! Unknown user ``" + user + "``...");
    }

}
