package com.rencredit.jschool.boruak.taskmanager.command.user.auth;

import com.rencredit.jschool.boruak.taskmanager.boorstrap.Bootstrap;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLogOutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout system.";
    }

    @Override
    public void execute() {
        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        authEndpoint.logOut(Bootstrap.getSession());
        session.setId("");
        session.setUserId("");
        session.setSignature("");
        session.setTimestamp(0L);
        System.out.println("Have been logout");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
