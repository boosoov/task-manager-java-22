package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.User;
import com.rencredit.jschool.boruak.taskmanager.endpoint.UserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

//import com.rencredit.jschool.boruak.taskmanager.entity.User;

public class UserEditProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "edit-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit profile.";
    }

    @Override
    public void execute() {
        System.out.println("Enter email");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        @NotNull final String middleName = TerminalUtil.nextLine();

        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @Nullable final String userId = authEndpoint.getUserId(session);
        @NotNull final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, userId, email, firstName, lastName, middleName);
        ViewUtil.showUser(user);

    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
