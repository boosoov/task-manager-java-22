package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ISessionEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.dto.Fail;
import com.rencredit.jschool.boruak.taskmanager.dto.Result;
import com.rencredit.jschool.boruak.taskmanager.dto.Success;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
    }

    public SessionEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Session openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        return sessionService.open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        try {
            sessionService.close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        try {
            sessionService.closeAll(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
