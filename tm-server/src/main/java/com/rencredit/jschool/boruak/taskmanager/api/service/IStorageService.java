package com.rencredit.jschool.boruak.taskmanager.api.service;

import java.io.IOException;

public interface IStorageService {

    void clearBase64() throws IOException;

    void loadBase64() throws IOException, ClassNotFoundException;

    void saveBase64() throws IOException;

    void clearBinary() throws IOException;

    void loadBinary() throws IOException, ClassNotFoundException;

    void saveBinary() throws IOException;

    void cleanJson() throws IOException;

    void loadJson() throws IOException;

    void saveJson() throws IOException;

    void clearXml() throws IOException;

    void loadXml() throws IOException;

    void saveXml() throws IOException;

}
