package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IAdminUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    private IUserService userService;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        userService = serviceLocator.getUserService();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Nullable
    @Override
    @WebMethod
    public User lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        return userService.lockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User unlockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        return userService.unlockUserByLogin(login);
    }

}
