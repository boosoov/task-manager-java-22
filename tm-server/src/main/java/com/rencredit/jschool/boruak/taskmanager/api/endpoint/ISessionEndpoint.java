package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.dto.Result;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    Session openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @NotNull
    @WebMethod
    Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    @WebMethod
    Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

}
