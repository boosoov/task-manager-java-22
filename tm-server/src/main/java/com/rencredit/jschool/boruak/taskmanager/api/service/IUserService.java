package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    @Nullable
    User getById(@Nullable String id);

    @Nullable
    User getByLogin(@Nullable String login);

    boolean add(@Nullable String login, @Nullable String password);

    boolean add(@Nullable String login, @Nullable String password, @Nullable String firstName);

    boolean add(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User editProfileById(@Nullable String id, @Nullable String firstName);

    @Nullable
    User editProfileById(@Nullable String id, @Nullable String firstName, @Nullable String lastName);

    @Nullable
    User editProfileById(@Nullable String id, @Nullable final String email, @Nullable String firstName, @Nullable String lastName, @Nullable final String middleName);

    @Nullable
    User updatePasswordById(@Nullable String id, @Nullable String newPassword);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByUser(@Nullable User user);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

}
