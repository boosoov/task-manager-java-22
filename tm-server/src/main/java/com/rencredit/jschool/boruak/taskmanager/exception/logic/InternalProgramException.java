package com.rencredit.jschool.boruak.taskmanager.exception.logic;

public class InternalProgramException extends RuntimeException {

    public InternalProgramException() {
        super("Internal program logic exception...");
    }

}
