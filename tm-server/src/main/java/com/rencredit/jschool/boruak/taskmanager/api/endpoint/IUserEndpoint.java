package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User getUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    User getUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    boolean addUserLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    boolean addUserLoginPasswordFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    );

    @WebMethod
    boolean addUserLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    );

    @Nullable
    @WebMethod
    User editUserProfileByIdFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    );

    @Nullable
    @WebMethod
    User editUserProfileByIdFirstNameLastName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName
    );

    @Nullable
    @WebMethod
    User editUserProfileByIdFirstNameLastNameMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "email", partName = "email") String email,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    );

    @Nullable
    @WebMethod
    User updateUserPasswordById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    );

    @Nullable
    @WebMethod
    User removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    User removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @Nullable
    @WebMethod
    User removeUserByUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "user", partName = "user") User user
    );

    @WebMethod
    void loadUserCollection(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") Collection<User> elements
    );

    @WebMethod
    void loadUserVararg(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") User... elements
    );

    @WebMethod
    boolean mergeUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "element", partName = "element") User element
    );

    @WebMethod
    void mergeUserCollection(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") Collection<User> elements
    );

    @WebMethod
    void mergeUserVararg(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") User... elements
    );

    @WebMethod
    void clearAllUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    @WebMethod
    List<User> getUserList(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

}
