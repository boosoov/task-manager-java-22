package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import com.rencredit.jschool.boruak.taskmanager.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository repository;

    public SessionService(@NotNull final IServiceLocator serviceLocator, @NotNull final ISessionRepository repository) {
        this.serviceLocator = serviceLocator;
        this.repository = repository;
    }

    @Override
    public void close(@Nullable final Session session) {
        validate(session);
        repository.remove(session);
    }

    @Override
    public void closeAll(@Nullable final Session session) {
        validate(session);
        repository.removeByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) {
        final String userId = getUserId(session);
        return serviceLocator.getUserService().getById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final Session session) {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getListSession(@Nullable final Session session) {
        validate(session);
        return repository.getList();
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) {
        if (session == null) throw new DeniedAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new DeniedAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new DeniedAccessException();
        if (session.getTimestamp() == null) throw new DeniedAccessException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new DeniedAccessException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new DeniedAccessException();
        if (!repository.contains(session.getId())) throw new DeniedAccessException();
    }


    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new DeniedAccessException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().getById(userId);
        if (user == null) throw new DeniedAccessException();
        if (!role.equals((user.getRole()))) throw new DeniedAccessException();
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        repository.merge(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;

        final User user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        final User user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) return;
        final String userId = user.getId();
        repository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeByUserId(userId);
    }

}
