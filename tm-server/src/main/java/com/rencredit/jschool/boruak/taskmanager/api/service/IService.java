package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    void load(@Nullable Collection<E> elements);

    void load(@Nullable E... elements);

    boolean merge(@Nullable E element);

    void merge(@Nullable Collection<E> elements);

    void merge(@Nullable E... elements);

    void clearAll();

    @NotNull
    List<E> getList();

}
