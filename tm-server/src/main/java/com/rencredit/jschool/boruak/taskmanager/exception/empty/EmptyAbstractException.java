package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyAbstractException extends RuntimeException {

    public EmptyAbstractException() {
        super("Error! Abstract is empty...");
    }

}
