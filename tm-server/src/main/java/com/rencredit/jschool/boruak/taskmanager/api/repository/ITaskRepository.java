package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    void add(@NotNull String userId, @NotNull Task task);

    @Nullable
    Task remove(@NotNull String userId, @NotNull Task task);

    void clearByUserId(@NotNull String userId);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeOneByName(@NotNull String userId, @NotNull String name);

}
