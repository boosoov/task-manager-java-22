package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class Task extends AbstractEntity implements Serializable {

    public static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    public Task(@NotNull final String title) {
        this.name = title;
    }

    public Task(@NotNull final String title, @NotNull final String description) {
        this.name = title;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

}
