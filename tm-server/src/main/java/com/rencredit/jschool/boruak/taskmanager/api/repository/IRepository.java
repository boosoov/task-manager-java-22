package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void clearAll();

    boolean add(@NotNull E record);

    boolean remove(@NotNull E record);

    @NotNull
    List<E> getList();

    void load(@NotNull Collection<E> elements);

    void load(@NotNull E... elements);

    boolean merge(@NotNull E element);

    void merge(@NotNull Collection<E> elements);

    void merge(@NotNull E... elements);

}
