package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyHashLineException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    @NotNull
    private static final String SEPARATOR_KEY = "ghdgh453ng673";

    private static final int ITERATOR_KEY = 27;

    private HashUtil() {
    }

    @Nullable
    public static String getHashLine(@Nullable final String line) {
        return saltHashLine(line);
    }

    @Nullable
    private static String saltHashLine(@Nullable final String line) {
        if (line == null || line.isEmpty()) throw new EmptyHashLineException();
        @Nullable String hashLine = line;
        for (int i = 0; i < ITERATOR_KEY; i++) {
            hashLine = hashLineMD5(SEPARATOR_KEY + line + SEPARATOR_KEY);
        }
        return hashLine;
    }

    @Nullable
    public static String hashLineMD5(@Nullable final String line) {
        if (line == null || line.isEmpty()) throw new EmptyHashLineException();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            @NotNull final byte[] byteArray = messageDigest.digest(line.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder stringBuilder = new StringBuilder();
            for (byte symbol : byteArray) {
                stringBuilder.append(Integer.toHexString((symbol & 0xFF) | 0x100), 1, 3);
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.getStackTrace();
        }
        return null;
    }

}
