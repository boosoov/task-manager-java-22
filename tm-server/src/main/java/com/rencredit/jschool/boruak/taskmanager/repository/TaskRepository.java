package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        add(task);
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String userId, @NotNull final Task task) {
        remove(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> userTasks = new ArrayList<>();
        for (@NotNull final Task task : getList()) {
            if (userId.equals(task.getUserId())) {
                userTasks.add(task);
            }
        }
        return userTasks;
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        @NotNull List<Task> tasks = getList();
        for (int i = 0; i < tasks.size(); i++) {
            if (userId.equals(tasks.get(i).getUserId())) {
                tasks.remove(tasks.get(i));
            }
        }
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Task task : getList()) {
            if (userId.equals(task.getUserId()) && id.equals(task.getId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Task> userTasks = new ArrayList<>();
        for (Task task : getList()) {
            if (userId.equals(task.getUserId())) userTasks.add(task);
        }
        return userTasks.get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Task task : getList()) {
            if (userId.equals(task.getUserId()) &&
                    name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new EmptyTaskException();
        remove(userId, task);
        return task;
    }

}
