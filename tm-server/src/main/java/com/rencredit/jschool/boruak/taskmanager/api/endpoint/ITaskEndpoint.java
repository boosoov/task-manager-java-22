package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTaskName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    void createTaskNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    void createTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    );

    @WebMethod
    void removeTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    );

    @NotNull
    @WebMethod
    List<Task> findAllTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void clearTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @Nullable
    @WebMethod
    Task findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    );

    @Nullable
    @WebMethod
    Task findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @NotNull
    @WebMethod
    Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @NotNull
    @WebMethod
    Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @Nullable
    @WebMethod
    Task removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    );

    @Nullable
    @WebMethod
    Task removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    void loadCollectionTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Collection<Task> elements
    );

    @WebMethod
    void loadVarargTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Task... elements
    );

    @WebMethod
    boolean mergeElementTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "element", partName = "element") final Task element
    );

    @WebMethod
    void mergeCollectionTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Collection<Task> elements
    );

    @WebMethod
    void mergeVarargTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Task... elements
    );

    @WebMethod
    void clearAllTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    @WebMethod
    List<Task> getListTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

}
