package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITaskEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private ITaskService taskService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        taskService = serviceLocator.getTaskService();
    }

    @Override
    @WebMethod
    public void createTaskName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void createTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), task);
    }

    public void removeTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), task);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return taskService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        return taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        return taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void loadCollectionTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Collection<Task> elements
    ) {
        sessionService.validate(session);
        taskService.load(elements);
    }

    @Override
    @WebMethod
    public void loadVarargTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Task... elements
    ) {
        sessionService.validate(session);
        taskService.load(elements);
    }

    @Override
    @WebMethod
    public boolean mergeElementTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "element", partName = "element") final Task element
    ) {
        sessionService.validate(session);
        return taskService.merge(element);
    }

    @Override
    @WebMethod
    public void mergeCollectionTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Collection<Task> elements
    ) {
        sessionService.validate(session);
        taskService.merge(elements);
    }

    @Override
    @WebMethod
    public void mergeVarargTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "elements", partName = "elements") final Task... elements
    ) {
        sessionService.validate(session);
        taskService.merge(elements);
    }

    @Override
    @WebMethod
    public void clearAllTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        taskService.clearAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getListTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return taskService.getList();
    }

}
