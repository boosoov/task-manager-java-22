package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session removeByUserId(@NotNull final String userId) {
        @Nullable final Session session = findByUserId(userId);
        if (session == null) throw new EmptyUserException();
        return removeBySession(session);
    }

    @Nullable
    @Override
    public Session removeBySession(@NotNull final Session session) {
        return (remove(session) ? session : null);
    }


    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId) {
        for (@NotNull final Session session : getList()) {
            if (userId.equals(session.getId())) return session;
        }
        return null;
    }

    @Override
    public boolean contains(@NotNull final String userId) {
        @Nullable final Session session = findByUserId(userId);
        return getList().contains(session);
    }

}
