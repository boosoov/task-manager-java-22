package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void create(@Nullable String userId, @Nullable Project project);

    void remove(@Nullable String userId, @Nullable Project project);

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId);

    void clearByUserId(@Nullable String userId);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
