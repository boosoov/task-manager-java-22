package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean add(@NotNull final User user) {
        return super.add(user);
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : getList()) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : getList()) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Nullable
    @Override
    public User removeByUser(@NotNull final User user) {
        return (remove(user) ? user : null);
    }

}
